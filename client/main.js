import Vue from 'vue'
import VueRouter from 'vue-router'
import AppLayout from './components/AppLayout.vue'
import {routes} from './route'
Vue.use(VueRouter)

const router = new VueRouter({
    routes
})
import { Meteor } from 'meteor/meteor'

Meteor.startup(function () {
  new Vue({
    router,
    render: h => h(AppLayout)
  }).$mount('app')
})
