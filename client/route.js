import Home from './components/Home.vue'
import Page from './components/Page.vue'
export const routes = [
  {path: '/', component: Home},
  {path: '/page', component: Page}
]
